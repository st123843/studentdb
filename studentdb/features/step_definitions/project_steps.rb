Given('There is a project') do
    @project = FactoryBot.create :project
    puts "Project Creared: #{@project.inspect}"
end

Given('the project has students') do
    s1 = FactoryBot.create :student, project: @project
    s2 = FactoryBot.create :student, project: @project
    @project.students.each do |s|
        puts "Got student #{s.inspect}"
    end
end

When('I visit the projects page') do
    visit '/projects'
    save_and_open_page
end

Then('I should see the project in the list') do
    expect(page).to have_content(@project.name)
end  

Then('I should see a show link for the project') do
    expect(page).to have_link('Show', href: project_path(@project))
end

When('I show the project details') do
    first(:link, 'Show').click
end

Then('I should see a list of students on the project') do
    @project.students.each do |student|
      expect(page).to have_content(student.name)
    end
end