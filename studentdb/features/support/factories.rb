#This will create a Project Class
FactoryBot. define do 
    factory :project do 
        name { 'FSAD' } 
        url { 'https://fsad.com' } 
    end

    #This will create Student Class
    factory :student do
        sequence(:name) {|n| "Student #{n}"}
        sequence(:studentid) {|n| (111 +n).to_s}
    end
end